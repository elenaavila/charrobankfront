# Root Image
FROM reramos011/cells

# Set env variables
#ARG BBVA_USER
#ARG API_KEY

# Root folder
WORKDIR /charrobank

# Copy local files
ADD . /charrobank

# Avoid permission problems
USER root

# Install dependencies
RUN npm install

#RUN touch ~/.bowerrc && \
#    echo '{ \n\
#     "timeout": 120000, \n\
#     "registry": "https://'"$BBVA_USER"':'"$API_KEY"'@globaldevtools.bbva.com/artifactory/api/bower/bower-repo", \n\
#     "resolvers": [ \n\
#         "bower-art-resolver" \n\
#     ] \n\
# }' > ~/.bowerrc

# Install bower components
RUN bower install --allow-root

# Listen port
EXPOSE 8001

RUN chown -R 1001 /charrobank && \
chgrp -R 0 /charrobank && \
chmod -R 777 /charrobank

USER 1001

# Start command
CMD ["cells", "app:serve", "-c", "prod.json"]
