(function(document) {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'login': '/',
      'dashboard': '/dashboard',
      'transactions': '/transactions',
      'operations': '/operations'
    }
  });
}(document));
